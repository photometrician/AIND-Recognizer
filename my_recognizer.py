import warnings
from asl_data import SinglesData


def recognize(models: dict, test_set: SinglesData):
    """ Recognize test word sequences from word models set

   :param models: dict of trained models
       {'SOMEWORD': GaussianHMM model object, 'SOMEOTHERWORD': GaussianHMM model object, ...}
   :param test_set: SinglesData object
   :return: (list, list)  as probabilities, guesses
       both lists are ordered by the test set word_id
       probabilities is a list of dictionaries where each key a word and value is Log Liklihood
           [{SOMEWORD': LogLvalue, 'SOMEOTHERWORD' LogLvalue, ... },
            {SOMEWORD': LogLvalue, 'SOMEOTHERWORD' LogLvalue, ... },
            ]
       guesses is a list of the best guess words ordered by the test set word_id
           ['WORDGUESS0', 'WORDGUESS1', 'WORDGUESS2',...]
   """
    # I referred to the QA below.
    # https://discussions.udacity.com/t/failure-in-recognizer-unit-tests/240082/3
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    probabilities = [None] * len(test_set.get_all_Xlengths())
    guesses = [None] * len(test_set.get_all_Xlengths())
    # TODO implement the recognizer
    for index, value in test_set.get_all_Xlengths().items():
        X, Xlengths = value
        probabilities[index] = dict()
        max_logL = float("-inf")
        best_word = None
        for word, model in models.items():
            try:
                logL = model.score(X, Xlengths)
                probabilities[index][word] = logL
                if logL > max_logL:
                    max_logL = logL
                    best_word = word
            except Exception as e:
                probabilities[index][word] = float("-inf")

        guesses[index] = best_word

    return probabilities, guesses
