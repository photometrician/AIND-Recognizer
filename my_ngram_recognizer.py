import warnings
from asl_data import SinglesData
from arpalm import ArpaLM
import string


def recognize_ngram(arpalm: ArpaLM, data_set: SinglesData, probabilities: dict):
    """ Recognize data_set word sequences by using n-gram and raw probabilities

   :param arpalm: language model
   :param data_set: SinglesData object
   :param probabilities: result of probabilities by my_recognizer.recognize() 
   :return: guesses
       guesses is a list of the best guess words ordered by the test set word_id
           ['WORDGUESS0', 'WORDGUESS1', 'WORDGUESS2',...]
   """
    # references
    #   http://www1.icsi.berkeley.edu/Speech/docs/HTKBook3.2/node179_mn.html
    #   https://github.com/cmusphinx/sphinxtrain/blob/master/python/cmusphinx/arpalm.py
    # bigram: P(wi | wi-1) = P(wi|G(wi)) * P(G(wi) | G(Wi-1))
    guesses = []
    for video_num in data_set.sentences_index:
        sentence_probabilities = [probabilities[i] for i in data_set.sentences_index[video_num]]
        prev_words = ['<s>']
        for i in range(len(sentence_probabilities)):
            word_probabilities = sentence_probabilities[i]
            gram_word_probabilities = dict()
            for word, probability in word_probabilities.items():
                if word[-1] in string.digits:
                    # remove tail number
                    word = word[0:(len(word) - 1)]
                # create n-gram word sequence
                current_words = list(prev_words)
                current_words.append(word)
                if len(current_words) > arpalm.n:
                    current_words = current_words[1:]
                # n-gram score
                score = arpalm.score(*list(reversed(current_words)))

                # if last word, average </s> case and not </s> case
                if i == len(sentence_probabilities) - 1:
                    # create n-gram word sequence containing '</s>'
                    current_words.append('</s>')
                    if len(current_words) > arpalm.n:
                        current_words = current_words[1:]
                    # n-gram score
                    last_word_score = arpalm.score(*list(reversed(current_words)))
                    # average
                    score = (last_word_score + score) / 2.0

                gram_word_probabilities[word] = score + probability
            # select max probability word
            max_probability_word = max(gram_word_probabilities, key=gram_word_probabilities.get)
            guesses.append(max_probability_word)
            prev_words.append(max_probability_word)
    return guesses
