import math
import statistics
import warnings

import numpy as np
from hmmlearn.hmm import GaussianHMM
from sklearn.model_selection import KFold
from asl_utils import combine_sequences

# regularizing term
# from 2.4. Bayesian Information Criterion in A Model Selection Criterion for Classification: Application to HMM Topology Optimization.
alpha = 1.0


class ModelSelector(object):
    '''
    base class for model selection (strategy design pattern)
    '''

    def __init__(self, all_word_sequences: dict, all_word_Xlengths: dict, this_word: str,
                 n_constant=3,
                 min_n_components=2, max_n_components=10,
                 random_state=14, verbose=False):
        self.words = all_word_sequences
        self.hwords = all_word_Xlengths
        self.sequences = all_word_sequences[this_word]
        self.X, self.lengths = all_word_Xlengths[this_word]
        self.this_word = this_word
        self.n_constant = n_constant
        self.min_n_components = min_n_components
        self.max_n_components = max_n_components
        self.random_state = random_state
        self.verbose = verbose

    def select(self):
        raise NotImplementedError

    def base_model(self, num_states):
        # with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        # warnings.filterwarnings("ignore", category=RuntimeWarning)
        try:
            hmm_model = GaussianHMM(n_components=num_states, covariance_type="diag", n_iter=1000,
                                    random_state=self.random_state, verbose=False).fit(self.X, self.lengths)
            if self.verbose:
                print("model created for {} with {} states".format(self.this_word, num_states))
            return hmm_model
        except:
            if self.verbose:
                print("failure on {} with {} states".format(self.this_word, num_states))
            return None


class SelectorConstant(ModelSelector):
    """ select the model with value self.n_constant

    """

    def select(self):
        """ select based on n_constant value

        :return: GaussianHMM object
        """
        best_num_components = self.n_constant
        return self.base_model(best_num_components)


class SelectorBIC(ModelSelector):
    """ select the model with the lowest Baysian Information Criterion(BIC) score

    http://www2.imm.dtu.dk/courses/02433/doc/ch6_slides.pdf
    Bayesian information criteria: BIC = -2 * logL + p * logN
    """

    def select(self):
        """ select the best model for self.this_word based on
        BIC score for n between self.min_n_components and self.max_n_components

        :return: GaussianHMM object
        """
        # I referred to the urls below.
        # https://en.wikipedia.org/wiki/Hidden_Markov_model#Architecture
        # https://discussions.udacity.com/t/number-of-parameters-bic-calculation/233235
        # https://stats.stackexchange.com/questions/12341/number-of-parameters-in-markov-model
        # https://github.com/scikit-learn/scikit-learn/blob/master/sklearn/mixture/gmm.py#L637
        # number of parameters = number of transition parameters + number of emission parameters
        # number of transition parameters = (number of states) * (number of states - 1)
        # number of emission parameters = (number of states) * (size of means + size of covariance)

        min_result = float("inf")
        best_model = None
        for num_states in range(self.min_n_components, self.max_n_components):
            model = self.base_model(num_states)
            try:
                logL = model.score(self.X, self.lengths)
                transition_param_num = num_states * (num_states - 1)
                cov_params = model.covars_.size
                mean_params = model.means_.size
                emission_param_num = cov_params + mean_params
                p = transition_param_num + emission_param_num
                result = -2 * logL + alpha * p * np.log(sum(self.lengths))
                # print(f"logL:{logL} p:{p} length:{self.lengths} xshape:{self.X.shape[0]} loglen:{np.log(self.lengths)} result:{result}")
                if best_model is None:
                    best_model = model
                if min_result > result:
                    min_result = result
                    best_model = model
            except:
                # failed to estimate. skip non-viable model.
                pass
        return best_model


class SelectorDIC(ModelSelector):
    ''' select best model based on Discriminative Information Criterion

    Biem, Alain. "A model selection criterion for classification: Application to hmm topology optimization."
    Document Analysis and Recognition, 2003. Proceedings. Seventh International Conference on. IEEE, 2003.
    http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.58.6208&rep=rep1&type=pdf
    DIC = log(P(X(i)) - 1/(M-1)SUM(log(P(X(all but i))
    '''

    def select(self):
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        import traceback
        # TODO implement model selection based on DIC scores
        max_logL = float("-inf")
        best_model = None
        for num_states in range(self.min_n_components, self.max_n_components):
            model = self.base_model(num_states)
            try:
                logL_i = model.score(self.X, self.lengths)
                score_sum = 0.0
                j_count = 0
                for word in self.words:
                    if word == self.this_word:
                        continue
                    xj, xj_lengths = self.hwords[word]
                    try:
                        score_sum += model.score(xj, xj_lengths)
                        j_count += 1
                    except:
                        pass

                if j_count > 0:
                    result = logL_i - alpha * score_sum / float(j_count)
                else:
                    result = logL_i

                if result > max_logL:
                    max_logL = result
                    best_model = model
            except Exception as e:
                # failed to estimate. skip non-viable model.
                pass
        return best_model


class SelectorCV(ModelSelector):
    ''' select best model based on average log Likelihood of cross-validation folds

    '''

    def select(self):
        # TODO implement model selection using CV
        max_logL = float("-inf")
        best_model = None
        for num_states in range(self.min_n_components, self.max_n_components):
            if len(self.sequences) >= 3:
                split_method = KFold()
            elif len(self.sequences) > 1:
                split_method = KFold(len(self.sequences))
            else:
                continue

            score_sum = 0.0
            count = 0.0
            splited = split_method.split(self.sequences)
            for cv_train_idx, cv_test_idx in split_method.split(self.sequences):
                training_sequences, training_lengths = combine_sequences(cv_train_idx, self.sequences)
                test_sequences, test_lengths = combine_sequences(cv_test_idx, self.sequences)
                try:
                    model = GaussianHMM(n_components=num_states, covariance_type="diag", n_iter=1000,
                                        random_state=self.random_state, verbose=False).fit(training_sequences,
                                                                                           training_lengths)
                    score_sum += model.score(test_sequences, test_lengths)
                    count += 1.0
                except:
                    # failed to estimate. skip invalid model.
                    pass
            if count > 0:
                result = score_sum / count
                if best_model is None:
                    best_model = model
                if max_logL < result:
                    max_logL = result
                    best_model = model
        return best_model
